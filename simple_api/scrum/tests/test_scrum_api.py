from .test_setup import TestSetUP


class TestApi(TestSetUP):

    def test_list_boards(self):
        response = self.client.get(self.list_boards_url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data[0]['name'], 'Test board')
        self.assertEqual(response.data[0]['description'], 'Best test board')
        self.assertEqual(response.data[0]['todo_count'], 1)

    def test_board_detail(self):
        response = self.client.get(self.board_detail_url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['name'], 'Test board')
        self.assertEqual(response.data['description'], 'Best test board')
        self.assertEqual(response.data['todos'][0]['title'], 'Test todo')

    def test_board_create(self):
        response = self.client.post(self.create_board_url, { 'name': 'POST board' })

        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data['name'], 'POST board')

    def test_board_update(self):
        response = self.client.put(self.update_board_url, { 'name': 'Updated board' })

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['name'], 'Updated board')

    def test_board_delete(self):
        response = self.client.delete(self.delete_board_url)

        self.assertEqual(response.status_code, 204)

    def test_list_todos(self):
        response = self.client.get(self.list_todos_url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data[0]['title'], 'Test todo')

    def test_list_uncompleted_todos(self):
        response = self.client.get(self.list_uncompleted_todos_url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, [])

    def test_todo_create(self):
        response = self.client.post(self.create_todo_url, { 'title': 'POST todo', 'board': '1' })

        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data['title'], 'POST todo')

    def test_todo_update(self):
        response = self.client.put(self.update_todo_url, { 'title': 'Updated todo', 'board': '1' })

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['title'], 'Updated todo')

    def test_todo_delete(self):
        response = self.client.delete(self.delete_todo_url)

        self.assertEqual(response.status_code, 204)
