from rest_framework.test import APITestCase
from django.contrib.auth.models import User
from django.urls import reverse
from scrum.models import Board, Todo
from rest_framework.authtoken.models import Token


class TestSetUP(APITestCase):

    def setUp(self):
        self.list_boards_url = reverse('list_boards')
        self.board_detail_url = reverse('board_detail', args=['1'])
        self.create_board_url = reverse('create_board')
        self.update_board_url = reverse('update_board', args=['1'])
        self.delete_board_url = reverse('delete_board', args=['1'])
        self.list_todos_url = reverse('list_todos')
        self.list_uncompleted_todos_url = reverse('uncompleted_todos')
        self.create_todo_url = reverse('create_todo')
        self.update_todo_url = reverse('update_todo', args=['1'])
        self.delete_todo_url = reverse('delete_todo', args=['1'])

        first_user = User.objects.create_user(
            username='khalid', email='khalid.jaz@gmail.com', password='top_secret')
        first_board = Board.objects.create(
            name='Test board', description='Best test board')
        first_todo = Todo.objects.create(
            title='Test todo', done=True, board=first_board)

        token, created = Token.objects.get_or_create(user=first_user)

        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

        return super().setUp()

    def tearDown(self):
        return super().tearDown()
