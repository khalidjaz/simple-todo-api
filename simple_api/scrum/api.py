from rest_framework import generics
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from .serializers import BoardSerializer, DetailBoardSerializer, TodoSerializer

from .models import Board, Todo


class ListBoardsAPI(generics.ListAPIView):
    queryset = Board.objects.all()
    serializer_class = BoardSerializer
    permission_classes = [IsAuthenticated]


class DetailBoardAPI(generics.RetrieveAPIView):
    queryset = Board.objects.all()
    serializer_class = DetailBoardSerializer
    permission_classes = [IsAuthenticated]


class CreateBoardApi(generics.CreateAPIView):
    queryset = Board.objects.all()
    serializer_class = BoardSerializer
    permission_classes = [IsAuthenticated]


class UpdateBoardApi(generics.RetrieveUpdateAPIView):
    queryset = Board.objects.all()
    serializer_class = BoardSerializer
    permission_classes = [IsAuthenticated]


class DeleteBoardApi(generics.DestroyAPIView):
    queryset = Board.objects.all()
    serializer_class = BoardSerializer
    permission_classes = [IsAuthenticated]


class ListTodosAPI(generics.ListAPIView):
    queryset = Todo.objects.all()
    serializer_class = TodoSerializer
    permission_classes = [IsAuthenticated]


class ListUncompletedTodosAPI(generics.ListAPIView):
    queryset = Todo.objects.filter(done=False)
    serializer_class = TodoSerializer
    permission_classes = [IsAuthenticated]


class CreateTodoAPI(generics.CreateAPIView):
    queryset = Todo.objects.all()
    serializer_class = TodoSerializer
    permission_classes = [IsAuthenticated]


class UpdateTodoAPI(generics.RetrieveUpdateAPIView):
    queryset = Todo.objects.all()
    serializer_class = TodoSerializer
    permission_classes = [IsAuthenticated]


class DeleteTodoAPI(generics.DestroyAPIView):
    queryset = Todo.objects.all()
    serializer_class = TodoSerializer
    permission_classes = [IsAuthenticated]
