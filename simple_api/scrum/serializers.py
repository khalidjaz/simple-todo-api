from rest_framework import serializers

from .models import Board, Todo


class TodoSerializer(serializers.ModelSerializer):

    class Meta:
        model = Todo
        fields = ['id', 'title', 'done', 'board', 'created_at', 'updated_at']


class BoardSerializer(serializers.ModelSerializer):

    class Meta:
        model = Board
        fields = ['id', 'name', 'description',
                  'todo_count', 'created_at', 'updated_at']


class DetailBoardSerializer(serializers.ModelSerializer):
    todos = TodoSerializer(many=True, read_only=True)

    class Meta:
        model = Board
        fields = ['id', 'name', 'description',
                  'todos', 'created_at', 'updated_at']
