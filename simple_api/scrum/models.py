from django.db import models
from django.db.models import Count


class Board(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField(blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def todo_count(self):
        return self.todos.count()

    def __str__(self):
        return "Board: {}".format(self.name)


class Todo(models.Model):
    title = models.CharField(max_length=200)
    done = models.BooleanField(default=False)
    board = models.ForeignKey(
        Board, related_name='todos', on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "Todo: {}".format(self.title)
