from django.urls import path

from .api import ListBoardsAPI, DetailBoardAPI, CreateBoardApi, UpdateBoardApi, DeleteBoardApi, ListTodosAPI, ListUncompletedTodosAPI, CreateTodoAPI, UpdateTodoAPI, DeleteTodoAPI

urlpatterns = [
    path('boards', ListBoardsAPI.as_view(), name='list_boards'),
    path('board/<int:pk>', DetailBoardAPI.as_view(), name='board_detail'),
    path('board/create', CreateBoardApi.as_view(), name='create_board'),
    path('board/<int:pk>/update', UpdateBoardApi.as_view(), name='update_board'),
    path('board/<int:pk>/delete', DeleteBoardApi.as_view(), name='delete_board'),
    path('todos', ListTodosAPI.as_view(), name='list_todos'),
    path('todos/uncompleted', ListUncompletedTodosAPI.as_view(), name='uncompleted_todos'),
    path('todo/create', CreateTodoAPI.as_view(), name='create_todo'),
    path('todo/<int:pk>/update', UpdateTodoAPI.as_view(), name='update_todo'),
    path('todo/<int:pk>/delete', DeleteTodoAPI.as_view(), name='delete_todo'),
]
