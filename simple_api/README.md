## Simple TODO API

Welcome to my first ever Django App!

Please run the tests covering all endpoints using the following command
* python manage.py test


I attached a json file that I exported from my Postman it has all the endpoints needed to test the API's

Please update the auth-token to your own user when using the Postman collection - it's not perfect but that's what I used in my dev env.

Please email me at khalid.jaz@gmail.com if you have any questions.

Kind Regards,
Khalid
